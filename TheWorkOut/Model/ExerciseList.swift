//
//  ExerciseList.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-06.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import Foundation
import Firebase

class ExerciseList {
    
    var exerciseList = [Exercise]()
    var workoutName : String = ""
    var workoutDuration : Float = 0.0
    var totalReps = 0
    var totalSets = 0
    var totalExercises = 0
    var workoutImage : String = ""
    
    var firebaseKey : String = ""

    
    func loadWorkouts(workoutInfo: DataSnapshot) {
        
        let workoutDictionery = workoutInfo.value as! NSDictionary
        
        firebaseKey = workoutInfo.key
        workoutName = workoutDictionery["workoutname"] as! String
        //let workoutDuration = workoutDictionery["workoutduration"] as! Float
        
        // Behövs om saker i firebase saknar duration
        if let duration = workoutDictionery["workoutduration"] as? Float {
            workoutDuration = duration
        } else {
            workoutDuration = 0.0
        }
    }
    
    
    func saveWorkout() {
        
        var ref = Database.database().reference()
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        let workoutDetails : [String : Any] = ["workoutname": workoutName, "workoutduration": workoutDuration, "totalreps": totalReps, "totalsets": totalSets, "totalexercises": totalExercises]
        
        ref.child("workoutplanner").child(user).child("workouts").child(firebaseKey).updateChildValues(workoutDetails)
    }
    
}
