//
//  Exercise.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2018-12-09.
//  Copyright © 2018 Henrik Jangefelt Nilsson. All rights reserved.
//

import Foundation
import Firebase

enum Muscle {
    
    case Chest
    case Back
    case Shoulders
    case Triceps
    case Biceps
    case Abs
    case Calves
    case Gluteus
    case Hamstrings
    case Quads
}

class Exercise {
    
//    var exerciseName : String
//    //var targetedMuscle : Muscle
//    var targetedMuscle : String
//    //var numberOfSets : Int
//    //var numberOfReps : Int
//    var numberOfSets = 3
//    var numberOfReps = 8
//
//    init(exerciseName : String, targetedMuscle : String, numberOfSets : Int, numberOfReps : Int) {
//
//        self.exerciseName = exerciseName
//        self.targetedMuscle = targetedMuscle
//        self.numberOfSets = numberOfSets
//        self.numberOfReps = numberOfReps
//
//    }
    
    var exerciseName = ""
    //var targetedMuscle : Muscle
    //var targetedMuscle
    //var numberOfSets : Int
    //var numberOfReps : Int
    var numberOfSets = 0
    var numberOfReps = 0
    var exerciseDuration = 0.0
    var firebaseKey = ""
    var backgroundImage = ""
    
    var parentWorkout = ExerciseList()
    
    
    
    // Load saved exercises
    func loadExercises(workout: ExerciseList, exerciseInfo: DataSnapshot) {
        
        parentWorkout = workout
        let exerciseDictionery = exerciseInfo.value as! NSDictionary
        
        exerciseName = exerciseDictionery["exercisename"] as! String
        numberOfSets = exerciseDictionery["amountsets"] as! Int
        numberOfReps = exerciseDictionery["amountreps"] as! Int
        
        firebaseKey = exerciseInfo.key
      
        // TODO: Lägg till reps, etc.. (i andra VC?)
    }
    
    
    func saveExercise() {
        
       var ref = Database.database().reference()
        
        guard let user = Auth.auth().currentUser?.uid else { return }

        let exerciseDetails : [String : Any] = ["exercisename": exerciseName, "amountreps": numberOfReps, "amountsets": numberOfSets]

        //ref.child("workoutplanner").child(user).child("workouts").child(workoutKey).child("exercises").child(exerciseKey).setValue(exerciseDetails)
        ref.child("workoutplanner").child(user).child("workouts").child(parentWorkout.firebaseKey).child("exercises").child(firebaseKey).updateChildValues(exerciseDetails)
        
        
        // INFO! The difference between setValue and updateValue; setValue removes everything in the database that is not part of the setValue update while updateValue only updates effected values.
    }

  
    
    
}
