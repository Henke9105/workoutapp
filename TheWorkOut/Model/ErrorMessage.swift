//
//  ErrorMessage.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-04.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import Foundation
import Firebase

class ErrorMessage {
    
    func handleError(_ error: Error,_ vc: UIViewController) {

        if let errorCode = AuthErrorCode(rawValue: error._code) {

            switch errorCode {
                
            case .emailAlreadyInUse:
                Alert.showEmailAlreadyInUserAlert(on: vc)
                
            case .missingEmail, .invalidEmail, .invalidSender, .invalidRecipientEmail:
               Alert.showInvalidEmailAlert(on: vc)
                
            case .userNotFound:
                Alert.showUserNotFoundAlert(on: vc)
                
            case .userDisabled:
                Alert.showUserDiabledAlert(on: vc)
                
            case .networkError:
                Alert.showNetworkErrorAlert(on: vc)
                
            case .weakPassword:
                Alert.showWeakPasswordAlert(on: vc)
                
            case .wrongPassword:
                Alert.showWrongPasswordAlert(on: vc)
            
            case .invalidUserToken:
                Alert.showInvalidUserTokenAlert(on: vc)
            default:
                Alert.showUnkownErrorAlert(on: vc)
            }
        }
    }
    
}
