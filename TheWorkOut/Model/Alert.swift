//
//  Alert.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-13.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

struct Alert {
    
    private static func showBasicAlert(on vc: UIViewController, with title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        // All alerts shown on the main thread
        DispatchQueue.main.async { vc.present(alert, animated : true) }
    }
    
    
     static func showTxtFldAlert(on vc: UIViewController, with title: String, message: String?, placeholderText: String?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addTextField { (textField: UITextField) -> Void in
            textField.placeholder = placeholderText
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            
            if let textField = alertController.textFields?[0] {
                
                if textField.text != "" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name.init("AlertTextField"), object: nil, userInfo: ["password" : textField.text!])
                    
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(action: UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        vc.present(alertController, animated: true, completion: nil)
        
    }
    
    
    static func showInvalidEmailAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Invalid Email", message: "Please enter a valid email address")
    }
    
    static func showEmailAlreadyInUserAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Email already in use", message: "Try using a different email or switch to login")
    }
    
    static func showUserNotFoundAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "User not found", message: "Please check your credentials or try again")
    }
    
    static func showUserDiabledAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Account Disabled", message: "This account have been disabled. Please contact support")
    }
    
    static func showNetworkErrorAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Network Error", message: "Check your connections and try again")
    }
    
    static func showWeakPasswordAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Weak Password", message: "Password to weak. Password must contain atleast 6 characters")
    }
    
    static func showWrongPasswordAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Incorrect Password", message: "Please enter the correct password")
    }
    
    static func showInvalidUserTokenAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Unable To Sign In", message: "Please sign in again")
    }
    
    // Error unkown
    static func showUnkownErrorAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Unkown Error", message: "Unkown error occurred please try again")
    }
    
    // Recovery password
    static func recoveryMailSentAlert(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Recovery Mail Sent", message: "Please check your inbox for instructions on how to reset your password")
    }
    
    
//    static func reEnterPassword(on vc: UIViewController) -> String {
//        var text = showTxtFldAlert(on: vc, with: "Enter Password", message: nil, placeholderText: "Password")
//        return text
//    }
    
    
    
}
