//
//  User.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-26.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import Foundation
import Firebase

class User {
    
    var userName = ""
    var userImage = ""
    var userID = ""
    var userEmail = ""
    
    
    func loadUser(userInfo: DataSnapshot) {
        
        let userDictionery = userInfo.value as! NSDictionary
        
        userName = userDictionery["username"] as! String
        
    }
    
    func saveUser() {
        
        var ref = Database.database().reference()
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        let userDetails : [String : Any] = ["username": userName, "email": userEmail, "userid": userID]
        
        ref.child("workoutplanner").child(user).updateChildValues(userDetails)
        
    }
    
}
