//
//  Colors.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-13.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

struct Colors {
    
    static let darkBlue = UIColor(red: 51.0/255.0, green: 67.0/255.0, blue: 82.0/255.0, alpha: 1.0)
    
    static let blue = UIColor(red: 58.0/255.0, green: 82.0/255.0, blue: 108.0/255.0, alpha: 1.0)
    
    
}
