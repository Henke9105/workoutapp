//
//  UIView+extension.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-13.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

extension UIView {
    
    // Rounded corners, borderwidth and color
    func roundedCorners(myRadius : CGFloat, borderWith: CGFloat?, myColor : UIColor?) {
        
        self.layer.cornerRadius = self.frame.size.height/myRadius
        self.layer.masksToBounds = true
        
        if let border = borderWith {
            self.layer.borderWidth = 2
        }
        
        if let color = myColor {
            self.layer.borderColor = color.cgColor
        }
    }
    
    // Shadows
    func shadowAround(myRadius : CGFloat, myOpacity : Float, myColor : UIColor, offSetHeight : Int, offSetWidth : Int) {
        
        self.layer.shadowColor = myColor.cgColor
        self.layer.shadowRadius = myRadius
        self.layer.shadowOpacity = myOpacity
        self.layer.shadowOffset = CGSize(width: offSetWidth, height: offSetHeight)
    }
    
    // Gradient
    func setGradientBackground(firstColor: UIColor, secondColor: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        
        // Diagonal gradient
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
