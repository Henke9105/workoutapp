//
//  ExerciseCell.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2018-12-10.
//  Copyright © 2018 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

class ExerciseCell: UITableViewCell {

    @IBOutlet weak var exerciseName: UILabel!
    @IBOutlet weak var amountOfSets: UILabel!
    @IBOutlet weak var amountOfReps: UILabel!
    @IBOutlet weak var targetedMuscle: UILabel!
    @IBOutlet weak var exerciseDuration: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    func setExercise(exercise : Exercise) {
        
        exerciseName.text = exercise.exerciseName
        amountOfSets.text = "\(exercise.numberOfSets)"
        amountOfReps.text = "\(exercise.numberOfReps)"
        exerciseDuration.text = "\(exercise.exerciseDuration)"
        //backgroundImage.image = UIImage(named: exercise.backgroundImage)
        
    }

}
