//
//  WorkoutCell.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-02.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

class WorkoutCell: UITableViewCell {

    @IBOutlet weak var workoutName: UILabel!
    @IBOutlet weak var amountOfExercises: UILabel!
    @IBOutlet weak var workoutDuration: UILabel!
    @IBOutlet weak var amountOfReps: UILabel!
    @IBOutlet weak var amountOfSets: UILabel!
    @IBOutlet weak var workoutImage: UIImageView!
    
    @IBOutlet weak var containerViewCell: UIView!
    
    func setWorkout(workout : ExerciseList) {
        
        containerViewCell.roundedCorners(myRadius: 5, borderWith: 2, myColor: .white)
        
        workoutName.text = workout.workoutName
        amountOfExercises.text = "\(workout.totalExercises)"
        amountOfReps.text = "\(workout.totalReps)"
        amountOfSets.text = "\(workout.totalSets)"
        workoutDuration.text = "\(workout.workoutDuration)"
        
    }
    
    
}
