//
//  VerificationVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-01-20.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

// TODO: kolla att mail slutar på hotmail.com/gmail.com etc. (string contains, slutar på)
class VerificationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var verificationBtnOutlet: UIButton!
    @IBOutlet weak var forgotPwdBtnOutlet: UIButton!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var bottomBtnOutlet: UIButton!
    @IBOutlet weak var passwordIcon: UIImageView!
    
    // Används inte för tillfället
    @IBOutlet weak var emailMarker: UIImageView!
    @IBOutlet weak var passwordMarker: UIImageView!
    
    var userSignIn = true
    var showPassword = false
    var errorMessage : ErrorMessage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardOnTap() // Removes keyboard
        errorMessage = ErrorMessage() // Class for handling login/sign up errors
        
        addImageTapGesture() // Image tap gesture
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Placeholder text for emailTextField
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        // Placeholder text for passwordTextField
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        // Background gradient
        view.setGradientBackground(firstColor: Colors.blue, secondColor: Colors.darkBlue)
        
        // Rounded corner for verification button
        verificationBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
        
        // Title for login button
        verificationBtnOutlet.setTitle("Login", for: .normal)
        
        // Buttom screen text
        bottomLabel.text = "Don't have an account? "
        
        // Buttom screen button title
        bottomBtnOutlet.setTitle("  Sign up", for: .normal)
        
        // Hiddes marker
        emailMarker.isHidden = true
        passwordMarker.isHidden = true
    }
    
    
    // Save user information
    func saveUser(email: String, userID: String) {
        let newUser = User()
        newUser.userEmail = email
        newUser.userID = userID
        newUser.saveUser()
    }
    
    // Register new user
    func createUser(email : String, password : String) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            
            guard let user = authResult?.user else {
                
                if let error = error {
                    // Error occurred, sign up failed
                    self.errorMessage?.handleError(error, self)
                }
                return
            }
            // Successfully created a user
            
            // Save users email and userID
            self.saveUser(email: email, userID: user.uid)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // Login user
    func signInUser(email : String, password : String) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            
            guard let user = authResult?.user else {
                
                if let error = error {
                    self.errorMessage?.handleError(error, self)
                }
                return
            }
            // Successfully logged in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // Handle pressing return on keyboard (jump to next TextField)
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            passwordTextField.resignFirstResponder()
        default:
            print("Error")
        }
        return true
    }
    
    // Show/hide password
    func makePasswordVisible() {
        
        showPassword = !showPassword
        
        passwordTextField.isSecureTextEntry = showPassword ? false : true
    }
    
    // Adds tap gesture to image
    func addImageTapGesture() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(gesture:)))
        
        passwordIcon.addGestureRecognizer(tapGesture)
    }
    
    // When image gets tapped
    @objc func imageTapped(gesture: UITapGestureRecognizer) {
        
        if gesture.view as? UIImageView != nil {
            
            makePasswordVisible()
        }
    }
    
    
    // Pressed login/sign up button
    @IBAction func verificationTapped(_ sender: Any) {
        
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        
        // If userSignIn is true: signInUser else createUser
        userSignIn ? signInUser(email: email, password: password) : createUser(email: email, password: password)
        
    }
    
    
    // Pressed forgot password
    @IBAction func forgotPwdTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Verification", bundle: Bundle.main)
        
        let otherVC = storyboard.instantiateViewController(withIdentifier: "ResetPassword") as! ResetPasswordVC
        self.present(otherVC, animated: true, completion:  nil)
    }
    
    
    // Pressed button for switching between login and register
    @IBAction func verificationChangeTapped(_ sender: Any) {
        
        userSignIn = !userSignIn
        
        var verificationBtnTitle = ""
        var verificationType = ""
        
        verificationBtnTitle = userSignIn ? "Login" : "Create Account"
        verificationBtnOutlet.setTitle(verificationBtnTitle, for: .normal)
        forgotPwdBtnOutlet.isHidden = userSignIn ? false : true
        bottomLabel.text = userSignIn ? "Don't have an account? " : "Already have a account? "
        verificationType = userSignIn ? "  Sign up" : "  Sign in"
        bottomBtnOutlet.setTitle(verificationType, for: .normal)
    }
   
        
}









//var showPassword = false

//@IBAction func showOrHidePassword(_ sender: Any) {
//    showPassword = !showPassword
//
//    passwordTextField.isSecureTextEntry = showPassword ? false : true
//
//    // showOrHideBtn.setTitle = showPassword ? ("Hide", for: .normal) : showOrHideBtn.setTitle("Show", for: .normal)
//
//    //        if showPassword {
//    //            showOrHideBtn.setTitle("Hide", for: .normal)
//    //        } else {
//    //            showOrHideBtn.setTitle("Show", for: .normal)
//    //        }
//}

