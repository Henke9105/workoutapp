//
//  ResetPasswordVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-04.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

class ResetPasswordVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var forgotPwdBtnOutlet: UIButton!
    var errorMessage : ErrorMessage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardOnTap() // Dissmiss keyboard on tap
        errorMessage = ErrorMessage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Background gradient
        view.setGradientBackground(firstColor: Colors.blue, secondColor: Colors.darkBlue)
        forgotPwdBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
    }

    @IBAction func forgotPwdTapped(_ sender: Any) {
        
        guard let email = emailTextField.text else { return }

        sendPasswordReset(email: email)
    }
    
    @IBAction func goBackTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // Reset password
    func sendPasswordReset(email : String) {
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            
            if error != nil {
                // Couldn't send recovery mail
                self.errorMessage?.handleError(error!, self)
                
            } else {
                // Recovery mail sent
                Alert.recoveryMailSentAlert(on: self)
            }
        }
    }
    

    
}
