//
//  WorkoutVC+TableView.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-24.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

extension WorkoutVC: UITableViewDataSource, UITableViewDelegate {
    
    
    // Number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workouts.count
    }
    
    
    // Content of cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let workout = workouts[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell") as! WorkoutCell
        
        cell.setWorkout(workout: workout)
                
        return cell
    }
    
    
    // Selecting a row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("didSelectRowAt")
        // Perform a segue to ExerciseVC
        performSegue(withIdentifier: "exerciseSegue", sender: indexPath.row)
       //print(ref.child("workouts").child(workouts[indexPath.row].firebaseKey))
    }
    
    
    // Do changes to a a row
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        print("editActionsForRowAt")
        
        // Edit cell
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            
            //self.performSegue(withIdentifier: "exerciseSegue", sender: indexPath.row)
            //TODO: code for editing
        }
        
        // Delete cell
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            
            self.deleteCell(indexPath: indexPath)
            self.loadWorkouts()
        }
        
        return [editAction, deleteAction]
    }
    
    
    // Enables moving cells
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    // Moving cell from -> to
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let workoutToMove = workouts[sourceIndexPath.row]
        workouts.remove(at: sourceIndexPath.row)
        workouts.insert(workoutToMove, at: destinationIndexPath.row)
    }
    
    
    
    
    // OPTIONAL
    // Height of a row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    // Sätt ut färg gradient på cellerna
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = colorForIndex(index: indexPath.row)
    }
    
}
