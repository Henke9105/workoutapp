//
//  ExerciseVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-25.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

// TODO: i viewwillappear sätt en label till att vara workoutens namn om det redan har angets (gör om txtfld + label ovanpå???)
// TODO: Sätt current workout till optional och kolla i view did load om den finns
// TODO: parent off child istället
//var ref = snapshot.ref
//parentWorkout = ref.parent?.key

class ExerciseVC: UIViewController, UITabBarDelegate {

    
    @IBOutlet weak var exerciseTableView: UITableView!
    @IBOutlet weak var exerciseNameTxtFld: UITextField!
    
    
    var currentWorkout = ExerciseList() // TODO gör till optional
    var exercises = [Exercise]()

    
    var ref : DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        self.title = currentWorkout.workoutName
        
        
        hideKeyboardOnTap()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadExercises()
    }
    

    // Gradient för tableveiw cellerna // TEST
    func colorForIndex(index: Int) -> UIColor {
        let itemCount = exercises.count - 1
        
        let greenColor = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        let redColor = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        let blueColor = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        return UIColor(red: redColor, green: greenColor, blue: blueColor, alpha: 1.0)
    }
   
    
   
    @IBAction func addExerciseBtn(_ sender: Any) {
        
        guard let exerciseName = exerciseNameTxtFld.text else { return }
        
        if exerciseName.isEmpty {
            showAlertMessage()
            return
        }
        
        // ANNAT TEST
        let randomID = Database.database().reference().childByAutoId()
        
        guard let autoID = randomID.key else { return }
        
        let newExercise = Exercise()
        newExercise.exerciseName = exerciseName
        newExercise.firebaseKey = autoID
        newExercise.parentWorkout = currentWorkout
        newExercise.saveExercise()
        
        
        
 
        
        
        
        // GAMLA VERSIONEN
        
//        guard let user = Auth.auth().currentUser?.uid else { return }
//
//        // perform segue??
//        
//        
//        let newExercise : [String : Any] = ["exercisename" : exerciseName]
//        
//        ref.child("workoutplanner").child(user).child("workouts").child(currentWorkout.firebaseKey).child("exercises").childByAutoId().setValue(newExercise)
        
        
        exerciseNameTxtFld.text = ""
        loadExercises()
    }
    
    
    func showAlertMessage() {
        let alert = UIAlertController(title: "Unnamed Workout", message: "Please give your workout a name to continue", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Okey", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    
    
    
    
    
    
    
    
    
    func loadExercises() {
       
        exercises.removeAll()  // Prevents adding same things to tableview each time function gets called
        
        var totalExercises = 0 // TEST!!!!!
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        // Fetch all data under "exercises"
        ref.child("workoutplanner").child(user).child("workouts").child(currentWorkout.firebaseKey).child("exercises").observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            // For every exercise (child) - and give each child as a snapshot
            for exercise in snapshot.children.allObjects as! [DataSnapshot] {
                
                // Inside a single exercise
                
                totalExercises += 1 // TEST!!!!
                
                // TODO: Parent of a child istället för workout: currentWorkout
            
                let newExercise = Exercise()
                newExercise.loadExercises(workout: self.currentWorkout, exerciseInfo: exercise)
                self.exercises.append(newExercise)
            }
            
            self.exerciseTableView.reloadData()
            
            print("amoutExercise \(totalExercises)") // TEST!!!!
            // TODO: save amount of exercises, reps, sets etc.

        }) { (error) in
            print(error.localizedDescription)
        }
    }

    // TODO: Add duration
    func saveWorkoutData(workoutKey : String, exerciseAmount : Int, totalSets : Int, totalReps : Int) {
        
    }
    
    func deleteCell(indexPath: IndexPath) {
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        ref.child("workoutplanner").child(user).child("workouts").child(currentWorkout.firebaseKey).child("exercises").child(exercises[indexPath.row].firebaseKey).removeValue()
    }
    
    // Vad som ska skickas med
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "exerciseDetail" {
            
            let destination = segue.destination as! DetailVC
            
            
            destination.currentExercise = exercises[sender as! Int]
            
    

           
            
        }
        
       
    }
    
}
