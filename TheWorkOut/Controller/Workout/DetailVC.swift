//
//  DetailVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-06.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

// TODO: alert ruta
//TODO: ladda ner relevant info?
// TODO: Add exercise funkar inte om man inte anger text i textfield
class DetailVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  

    @IBOutlet weak var addExerciseBtnOutlet: UIButton!
    @IBOutlet weak var exerciseNameTxtFld: UITextField!
    @IBOutlet weak var setsLabel: UILabel!
    @IBOutlet weak var repsLabel: UILabel!
    @IBOutlet weak var targetedMuscleLabel: UILabel!
    @IBOutlet weak var checkMarkImage: UIImageView!
    @IBOutlet weak var musclePickerView: UIPickerView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var currentExercise = Exercise()
    
    let muscles : [Muscle] = [.Chest, .Back, .Shoulders, .Biceps, .Triceps, . Abs, .Gluteus, .Hamstrings, .Quads, .Calves] // Array of enum Muslce
    
    let targetedMuscles = ["Chest", "Back", "Shoulders", "Biceps", "Triceps", "Abs", "Calves", "Gluteus", "Hamstrings", "Quads"]
    
    //let targetMuscle = [Muscle]()

    //var targetedMuscle = Muscle
    // tränade musklen är targetedMuscle = Muscle.chest
    
    
    var amountOfReps = 0
    var amountOfSets = 0
    
    // Ändra till default pic av något slag
    var exerciseImage : UIImage = UIImage(named: "lock")!
    var ref : DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        hideKeyboardOnTap()
        checkMarkImage.isHidden = true
        
        amountOfReps = currentExercise.numberOfReps
        amountOfSets = currentExercise.numberOfSets
        
        exerciseImage.downloadImage(key: currentExercise.firebaseKey, uploadName: "exercisePic") {(result: UIImage?) in
            print("Tjohej")
            if let image = result {
                print("image")
                self.backgroundImage.image = image
            } else {
                // No image
                print("No image")
                // TODO: lägg logiken i modeln (pia08osv5tors2: ca 50min)
            }
        }

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Rounded corners on add Exercise Button
        addExerciseBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
        
        // exercisename textfield placeholder text
        exerciseNameTxtFld.attributedPlaceholder = NSAttributedString(string: currentExercise.exerciseName, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.title = currentExercise.exerciseName
        repsLabel.text = "\(currentExercise.numberOfReps)"
        setsLabel.text = "\(currentExercise.numberOfSets)"
        
        
        musclePickerView.setValue(UIColor.white, forKey: "textColor")
    }
    


    

    
    
    @IBAction func addExerciseButton(_ sender: Any) {
        addExercise()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            exerciseNameTxtFld.resignFirstResponder()
            return true
    }
    
    

    func addExercise() {
        // TODO: returnerar ur funktionen om man inte ändrar/skriver i textfield (är tom) även fast text finns i placeholdern
        
        if exerciseNameTxtFld.text == "" { return }
        guard let exercise = exerciseNameTxtFld.text else { return }
        
        currentExercise.exerciseName = exercise
        currentExercise.numberOfSets = amountOfSets
        currentExercise.numberOfReps = amountOfReps
        currentExercise.saveExercise()
        
        
//        if exerciseNameTxtFld.text == "" {
//            return
//        }
        
        //guard let exerciseName = exerciseNameTxtFld.text, let targetedMuscle = targetedMuscleLabel.text else {
          //  return
        //}
      
        
        //let newExercise = Exercise(exerciseName: exerciseName, targetedMuscle: targetedMuscle, numberOfSets: amountOfSets, numberOfReps: amountOfReps)
        
        
        
//        guard let user = Auth.auth().currentUser?.uid else { return }
//
//        let exerciseDetails : [String : Any] = ["amountofreps" : amountOfReps, "amountSets" : amountOfSets]
//        let key = currentExercise.parentWorkout.firebaseKey
        //ref.child("workoutplanner").child(user).child("workouts").child(workoutKey).child("exercises").child(exerciseKey).setValue(exerciseDetails)
//        ref.child("workoutplanner").child(user).child("workouts").child(key).child("exercises").child(currentExercise.firebaseKey).updateChildValues(exerciseDetails)
        
        
        //TODO: GO back to tableview
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    @IBAction func changeSetsAmountTapped(_ sender: UIButton) {
        
        amountOfSets = incrementAmount(tag: sender.tag, currentAmount: amountOfSets)
        setsLabel.text = "\(amountOfSets)"
        
    }
    
    @IBAction func changeRepsAmountTapped(_ sender: UIButton) {

        amountOfReps = incrementAmount(tag: sender.tag, currentAmount: amountOfReps)
        repsLabel.text = "\(amountOfReps)"
        
    }
    
    // Increases and decreases input amount based on button pressed
    func incrementAmount(tag : Int, currentAmount : Int) -> Int {
        
        var amount = currentAmount
        // Increase
        if tag == 1 && currentAmount > 1 {
            amount -= 1
        }
        // Decrease
        if tag == 2 && currentAmount < 99 {
            amount += 1
        }
        return amount
    }
    
    
    
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let pickedImage = info[.editedImage] as! UIImage
        backgroundImage.image = pickedImage
        
        pickedImage.uploadImage(image: pickedImage, uploadName: "exercisePic", key: currentExercise.firebaseKey) {(result: Bool) in
            
            if result {
                print("Image upload OK")
            } else {
                print("Image upload Failed!")
            }
        }
        
//        currentExercise.uploadImage(image: pickedImage) {(result: Bool) in
//            if result {
//                print("Uploading OK")
//            } else {
//                print("Upload Failed!")
//            }
//        }
        
        
     
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func gallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func camera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func testUpload(_ sender: Any) {
        gallery()
    }
    
}
