//
//  DetailVC+Picker.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-03-02.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

extension DetailVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    // Number of components/columns in pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Number of rows in pickerView
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return targetedMuscles.count
    }
    
    // Text for rows
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return targetedMuscles[row]
    }
    
    // Selected row
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        targetedMuscleLabel.text = targetedMuscles[row]
    }
    
}
