//
//  ExerciseVC+Tableview.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-25.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

extension ExerciseVC: UITableViewDataSource, UITableViewDelegate {
    
    // Number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return exercises.count
        //return currentWorkout.exerciseList.count //???
    }
    
    // Content of cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let exercise = exercises[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell") as! ExerciseCell
        
        cell.setExercise(exercise: exercise)

        
        // Download image
//        exercises[indexPath.row].downloadImage() {(result: UIImage?) in
//            cell.backgroundImage.image = result
//        }
        
        return cell
    }
    
    // Tryckte på en rad
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //print("Tryckte på rad \(indexPath.row)")
        performSegue(withIdentifier: "exerciseDetail", sender: indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            
            self.deleteCell(indexPath: indexPath)
            self.loadExercises()
        }
        return [deleteAction]
    }
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    
    
    
    // Sätt ut färg gradient på cellerna // TEST
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = colorForIndex(index: indexPath.row)
    }
    
}
