//
//  SignUpViewController.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-01-20.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

class VerificationVC: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailMarker: UIImageView!
    @IBOutlet weak var passwordMarker: UIImageView!
    @IBOutlet weak var verificationBtnOutlet: UIButton!
    
    var userSignIn = true
    var showPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardOnTap()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        verificationBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
        
        verificationBtnOutlet.setTitle("Sign In", for: .normal)
        emailMarker.isHidden = true
        passwordMarker.isHidden = true
    }
    
    
    
    @IBAction func verificationBtn(_ sender: Any) {
    }
    
    
    @IBAction func switchModeBtn(_ sender: Any) {
        
        userSignIn = !userSignIn
        
        var title = ""
        
        title = userSignIn ? "Sign In" : "Sign Up"
        verificationBtnOutlet.setTitle(title, for: .normal)
            
            
        
     //"Already a memeber?" "  Click here to sign in"
        //"  Click here to become one"
    }
    
    
    
    

    @IBAction func createAccountBtn(_ sender: Any) {
        
     signupUser()
        
        /*Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            
            
            //guard let user = authResult?.user else { return }
            
            if let theError = error {
                // Error hände
                print(theError)
            } else {
                // Sign up godkänd
            }
        }*/
    }
    
    
    
    
    // OM ERROR
    /*ref.child("users").child(user.uid).setValue(["username" : username]) {
    (error:Error?, ref: DatabaseReference) in
    if let error = error {
    print("Data could not be saved: \(error),")
    } else {
    print("Data saved successfully!")
    }
    }*/

    
    
    
    
    @IBAction func showOrHidePassword(_ sender: Any) {
        showPassword = !showPassword
        
        passwordTextField.isSecureTextEntry = showPassword ? false : true
        
       // showOrHideBtn.setTitle = showPassword ? ("Hide", for: .normal) : showOrHideBtn.setTitle("Show", for: .normal)
        
//        if showPassword {
//            showOrHideBtn.setTitle("Hide", for: .normal)
//        } else {
//            showOrHideBtn.setTitle("Show", for: .normal)
//        }
    }
    
    
    func signupUser() {
        
//        guard let email = emailTextField.text, let password = passwordTextField.text else {
//            return
//        }
//        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
//
//            guard let user = authResult?.user else {
//                self.warningLabel.isHidden = false
//                return
//            }
//            print("Successfully created user")
//
//            // Stäng view controllerna (efter att ha öppnat något modally)
//            self.dismiss(animated: false, completion: nil)
        
            /*if let theError = error {
                // Error
                print("Error")
            } else {
                print("Lyckades signa up")
            }*/
            
        //}
    }
    
    
    
    
    
    
   
    
    
}






//warningLabel.isHidden = true



/*
 var ref : DatabaseReference!
 ref = Database.database().reference()
 
 // Spara data
 ref.child("firstname").setValue("Henke")
 ref.child("people").child("someone").child("fullname").setValue("Torsen Torsenson")
 ref.child("people").child("someone").child("adress").setValue("Torstens vägen")
 
 
 // Hämta data
 // ObserveSingleEvent (hämta en gång)
 // koden mellan { } är kod som ska ske efter man fått svar
 // error, gick inte att hämta
 ref.child("people").child("someone").observeSingleEvent(of: . value, with: { (snapshot) in
 
 
 let value = snapshot.value as? NSDictionary
 let thename = value?["fullname"] as? String
 
 print(value)
 print(thename)
 
 //let username = value?["username"] as? String ?? ""
 //let user = User(username: username)
 
 }) { (error) in
 print(error.localizedDescription)*/
//}

// Hämta data
// Kollar om användaren är inloggad
//let userID = Auth.auth().currentUser?.uid
/*ref.child("user").child(userID!).observeSingleEvent(of: . value, with: { (snapshot) in
 
 let value = snapshot.value as? NSDictionary
 let username = value?["username"] as? String ?? ""
 let user = User(username: username)
 
 }) { (error) in
 print(error.localizedDescription)
 }*/
