//
//  TabBarVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-04.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

// TODO: Döp om till TabBarC (inte en View Controller!)
class TabBarVC: UITabBarController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        print("Current user: \(Auth.auth().currentUser)")
//        if Auth.auth().currentUser != nil {
//            self.segueToStart()
//        }
        
        Auth.auth().addStateDidChangeListener { auth, user in
            
            print("Current user: \(Auth.auth().currentUser)")
            if let user = user {
                // User is signed in.
                // Hämta data !!
            } else {
                // User in not signed in.
                print("No User")
                self.segueToStart()
            }
        }
        
        
        //self.selectedIndex = 0 // Which tab is presented when login
        self.tabBarController?.selectedIndex = 0

        
        
        
        
        
        
//        if Auth.auth().currentUser == nil {
//            self.performSegue()
//        }
        reloadUser()
        sendEmailVerification()
        print("Verified: \(Auth.auth().currentUser?.isEmailVerified)")
    }
    
    // Send email verification to user
//    func sendEmailVerification(_ callback: ((Error?) -> ())? = nil) {
//        Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
//            callback?(error)
//        })
//    }
    
//    // Reload user for updating verification
//    func reloadUser(_ callback: ((Error?) -> ())? = nil) {
//        Auth.auth().currentUser?.reload(completion: { (error) in
//            callback?(error)
//        })
//    }
    
    
    
    
    func sendEmailVerification() {
        guard let user = Auth.auth().currentUser else { return }
        
        // Skickar varje gång???
        if !user.isEmailVerified {
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                if error != nil {
                    // kunde inte skicka
                } else {
                    // Skicade verification email
                }
            })
        }
        
        //reloadUser()
    }
    
    func reloadUser() {
        Auth.auth().currentUser?.reload(completion: { (error) in
            if error != nil {
                // kunde inte reloada
            } else {
                // reloadade data
            }
        })
    }

    func segueToStart() {
        let storyboard = UIStoryboard(name: "Verification", bundle: Bundle.main)
        
        let otherVC = storyboard.instantiateViewController(withIdentifier: "Verification") as! VerificationVC
        self.present(otherVC, animated: false, completion:  nil)
    }

}
