//
//  StartViewController.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-03.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

// TODO: lägg loading i workout??
class DailyVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var beginWorkoutBtnOutlet: UIButton!
    
    @IBOutlet weak var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       loadingView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
       containerView.roundedCorners(myRadius: 30, borderWith: nil, myColor: nil)
        beginWorkoutBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
         // Sätt email som title
         //self.title = Auth.auth().currentUser?.email
         
        loadingView.isHidden = true
    }
    
 
   
 
    
    

}
