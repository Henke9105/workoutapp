//
//  ProfileVC+Camera.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-14.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

// TODO: Kolla om användaren har kamera (Ipad har inte)
// TODO: hantera om användaren tryckt tillåt inte
extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // Choose Image from gallery
    func chooseGalleryImage() {
        
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // take picture with camera
    func takePicture() {
        
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // After choosen a picture/ Uploading
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let pickedImage = info[.editedImage] as! UIImage
        let resizedImage = pickedImage.resizeImage(targetSize: CGSize(width: 500, height: 500))

        profileImage.image = resizedImage
        
        // Image uploading
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        let imageFile = storageRef.child("profilepic/"+user)
        
        
        guard let pngFile = resizedImage.pngData() else { return }
        
        // metadata, om något ska skickas med (plats, datum)
        imageFile.putData(pngFile, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else { return }
            
            print("Upload ok")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func downloadImage() {
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let imageFile = storageRef.child("profilepic/"+user)
        
        imageFile.getData(maxSize: 100 * 1024 * 1024) { (data, error) in
            
            if let error = error {
                // Error occurred!
                print("Download profilepic failed")
            } else {
                // Data for image is returned
                let downloadedImage = UIImage(data: data!)
                
                // Behövs/fungerar?
                DispatchQueue.main.async {
                    self.profileImage.image = downloadedImage
                }
            }
        }
        
    }
    
    
    
}
