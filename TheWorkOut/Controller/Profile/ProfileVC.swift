//
//  ProfileVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-05.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase
//TODO: kombinera action sheet funktion mws alert class (title, message som optional)

class ProfileVC: UIViewController  {

    
    @IBOutlet weak var userNameBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var testView: UIView!
    
    
    var userImage : UIImage = UIImage(named: "userIcon")!
    
    var ref: DatabaseReference!
    //var user: User?
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        //user = User()
        
        //userImage.downloadImage(key: <#T##String#>, uploadName: <#T##String#>, completion: <#T##(UIImage?) -> Void#>)
        downloadImage()
        
        //loadUser() // TEST
        
        addGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        profileImage.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
    }
    
    
    // Add touch gesture to image
    func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(gesture:)))
        profileImage.addGestureRecognizer(tapGesture)
    }
    
    
    // Code for when image tapped
    @objc func imageTapped(gesture : UIGestureRecognizer) {
        
        if gesture.view as? UIImageView != nil {
            showActionSheet()
        }
    }

    
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let gallery = UIAlertAction(title: "Gallery", style: .default) { action in

            self.chooseGalleryImage()
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { naction in

            self.takePicture()
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func nameAlert() {
        
        let alertController = UIAlertController(title: "Please enter your name", message: nil, preferredStyle: .alert)
        alertController.addTextField { (textField: UITextField) -> Void in
            textField.placeholder = "Enter name"
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
    
            if let textField = alertController.textFields?[0] {
                
                if textField.text != "" {
                    
                    self.userNameBtn.setTitle(textField.text, for: .normal)
                    self.saveUser(name: textField.text!)
                }
            }
        })
    
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    
    
    func loadUser() {
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        ref.child("workoutplanner").child(user).observeSingleEvent(of: .value) { (snapshot) in
            
            for user in snapshot.children.allObjects as! [DataSnapshot] {
                
                let newUser = User()
                newUser.loadUser(userInfo: user)
            }
        }
    }
    
    
    func saveUser(name: String) {
        
//        guard let user = Auth.auth().currentUser else { return }
//        let userID = user.uid
//        guard let userEmail = user.email else { return }
        
        let newUser = User()
        newUser.userName = name
//        newUser.userID = userID
//        newUser.userEmail = userEmail
        newUser.saveUser()
    }
    
    @IBAction func tappedUserName(_ sender: Any) {
        nameAlert()
    }
    
   
    
}
