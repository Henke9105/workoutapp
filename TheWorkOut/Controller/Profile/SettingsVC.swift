//
//  SettingsVC.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-06-04.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit
import Firebase

class SettingsVC: UIViewController {

    @IBOutlet weak var testLabel: UILabel!
    
    @IBOutlet weak var deleteBtnOutlet: UIButton!
    @IBOutlet weak var logoutBtnOutlet: UIButton!
    
    var alert : Alert?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        alert = Alert()
        notificationObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        deleteBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
        logoutBtnOutlet.roundedCorners(myRadius: 2, borderWith: nil, myColor: nil)
    }
    

    @IBAction func deleteAccountTapped(_ sender: Any) {
        
        //deleteUser(password: <#String#>)
        //let text = Alert.reEnterPassword(on: self)
        //let text =
        Alert.showTxtFldAlert(on: self, with: "Enter Password", message: nil, placeholderText: "Password")
        
    }
    
    
    @IBAction func signOutTapped(_ sender: Any) {
        
        signOutUser()
    }
    
    
    
    
    func notificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(onReceive(_:)), name: NSNotification.Name.init("AlertTextField"), object: nil)
    }
    
    @objc func onReceive(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            
            if let password = dict["password"] as? String {
                
                // DELETE ACCOUNT
                //deleteUser(password: password)
                testLabel.text = password
            }
            
        }
    }

    
    
    // Signout user
    func signOutUser() {
        
        do {
            try! Auth.auth().signOut()
            // Logout successful
        } catch let signOutError as NSError {
            // Failed to logout
            print("Failed to logut: \(signOutError)")
        }
    }
    
    
    // Delete user
    func deleteUser(password : String) {
        
        guard let currentUser = Auth.auth().currentUser else { return }
        
        let userID = currentUser.uid
    
        guard let userEmail = currentUser.email else { return }
        
        let userCredentials = EmailAuthProvider.credential(withEmail: userEmail, password: password)
       
        currentUser.reauthenticateAndRetrieveData(with: userCredentials) { (result, error) in
            
            if error != nil {
                
                // Remove from firebase
                let userData = Database.database().reference().child("workoutplanner").child(userID)
                userData.removeValue()
            }
        }
        
        
        
    }
    
}
