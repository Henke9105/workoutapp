//
//  WorkoutVC.swift
//  
//
//  Created by Henrik Jangefelt on 2019-02-24.
//

import UIKit
import Firebase

// TODO: alert ruta - namge workout?
// TODO: Slå samman delar av funktionerna addworkout, delete, loadworkouts??
// TODO: Sort by, action sheet??
// TODO: EDit knappen i slide funktionen, bytt namn på workout
class WorkoutVC: UIViewController {
    
    @IBOutlet var sortByBtns: [UIButton]!
    
    @IBOutlet weak var workoutTableView: UITableView!
    @IBOutlet weak var exerciseImage: UIImageView!
    @IBOutlet weak var editBtnOutlet: UIBarButtonItem!
    
    var workouts = [ExerciseList]() // Array of workouts
    
    var ref : DatabaseReference!

    var savedText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        loadWorkouts()
        
        imageTapGesture()
        
        self.workoutTableView.separatorColor = UIColor.clear


    }
    
    // Gradient för tableveiw cellerna //TEST
    func colorForIndex(index: Int) -> UIColor {
        let itemCount = workouts.count - 1
        let color = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        return UIColor(red: 1.0, green: color, blue: 0.0, alpha: 1.0)
    }
    
    
    
    // Test!!
    @IBAction func handleSortBySelection(_ sender: Any) {
        sortByBtns.forEach { (button) in
            UIView.animate(withDuration: 0.3, animations: {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
        }
    }
    
    enum SortBy: String {
        
        case fromAtoZ = "a-z"
        case fromZtoA = "z-a"
        case from1to0 = "1-0"
        case from0to1 = "0-1"
    }
    
    @IBAction func sortByTapped(_ sender: UIButton) {
   
        guard let title = sender.currentTitle, let sortBy = SortBy(rawValue: title) else { return }
        
        switch sortBy {
        case .fromAtoZ:
            workouts.sort(by: { $0.workoutName < $1.workoutName})
            print("A-z")
        case .fromZtoA:
            print("z-a")
            workouts.sort(by: { $0.workoutName > $1.workoutName})
        case .from0to1:
            print("0-1")
            workouts.sort(by: { $0.totalExercises < $1.totalExercises})
        case .from1to0:
            print("1-0")
            workouts.sort(by: { $0.totalExercises > $1.totalExercises})

        default:
            print("error")
        }
        
        workoutTableView.reloadData()
    }
    
    
    
    //--------------


    
    
        

    // Removes selected workout
    func deleteCell(indexPath : IndexPath) {
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        ref.child("workoutplanner").child(user).child("workouts").child(workouts[indexPath.row].firebaseKey).removeValue()
    }
    
    // Loads all workouts
    func loadWorkouts() {
    
        // Prevents adding same things to tableview each time function gets called
        workouts.removeAll()
        
        guard let user = Auth.auth().currentUser?.uid else { return }

        // Fetch all data under workouts
        ref.child("workoutplanner").child(user).child("workouts").observeSingleEvent(of: .value, with: { (snapshot) in
            
            // For every workout (child, given as a snapshot)
            for workout in snapshot.children.allObjects as! [DataSnapshot] {
                
                // Inside a single workout
                
                let newWorkout = ExerciseList()
                newWorkout.loadWorkouts(workoutInfo: workout)
                self.workouts.append(newWorkout)
            }
            
            self.workoutTableView.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    // Adds workout
    func addWorkout(workoutName : String) {
        
        var duration = 0.0
        
        guard let user = Auth.auth().currentUser?.uid else { return }
        
        let newWorkout : [String : Any] = ["workoutname" : workoutName, "workoutduration" : duration]
        
        ref.child("workoutplanner").child(user).child("workouts").childByAutoId().setValue(newWorkout)
        
        loadWorkouts()
    }
    
    
    
    
    
    
    
    
    // Lägg till cancel, kallal på funktionen addworkout när okej trycks
    func alertMessage() {
        let alert = UIAlertController(title: "Workout Name", message: "Please enter the name of your workout", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            //textField.text = "Some default text"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert](_) in
            
            let textField = alert?.textFields![0]
            print("Text \(textField?.text)")
            
            guard let text = textField?.text else { return }
            self.savedText = text
            self.addWorkout(workoutName: self.savedText)
        }))
        
        self.present(alert, animated : true, completion: nil)
    }
    

    
    
    
    
    
    
    
    
    // Sends data throu segue (any segue)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "exerciseSegue" {
            
            let destination = segue.destination as! ExerciseVC
            
            destination.currentWorkout = workouts[sender as! Int]
            
        }
        
        
        
            // Destinations workoutsArray är lika med denna vyns workoutArray
            //destination.workoutsArray = workoutsArray
            
            // Ge radnumret som en Int?
            //destination.workoutsArray = workoutsArray[sender as! Int]
            
            //destination.workoutName = workoutNameTextField.text
            /* dest.allExercises = exerciseList
             dest.currentExerciseNumber = sender as! Int*/
        
    }
    
    
    
    // TODO: ta bort
    @IBAction func handleSelection(_ sender: Any) {
        
        // Sort by alphabetic order (a-z)
        workouts.sort(by: { $0.workoutName < $1.workoutName})
        
        // Sort by alphabetic order (z-a)
        //workouts.sort(by: { $0.workoutName > $1.workoutName})
        
        // Sort by amount of exercises?
        //workouts.sort(by: $0.totalExercises < $1.totalExercises)
        
        self.workoutTableView.reloadData()
        
    }
    
    
    // Makes tableview editable (enables tableView cells to be moved)
    @IBAction func editingTapped(_ sender: Any) {
        
        editBtnOutlet.title = workoutTableView.isEditing ? "Edit" : "Done"
        
        workoutTableView.isEditing = !workoutTableView.isEditing
    }
    
    
    func imageTapGesture() {
        
        let exerciseTapGesture = UITapGestureRecognizer(target: self, action: #selector(exerciseImageTapped(gesture:)))
        exerciseImage.addGestureRecognizer(exerciseTapGesture)
    }
    
    @objc func exerciseImageTapped(gesture : UIGestureRecognizer) {
        
        if gesture.view as? UIImageView != nil {
            //performSegue(withIdentifier: "exerciseSegue", sender: self)
            alertMessage()
        }
    }
    

    @IBAction func goBackTapped(_ sender: Any) {

        dismiss(animated: true, completion: nil)
    }
    
}



