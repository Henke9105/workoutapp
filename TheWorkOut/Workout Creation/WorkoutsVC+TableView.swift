//
//  WorkoutVC+TableView.swift
//  TheWorkOut
//
//  Created by Henrik Jangefelt on 2019-02-24.
//  Copyright © 2019 Henrik Jangefelt Nilsson. All rights reserved.
//

import UIKit

extension WorkoutVC: UITableViewDataSource, UITableViewDelegate {
    
    // Number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return workoutsArray.count
        //return workoutsArray[section].count
    }
    
    // Content of cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell") as! WorkoutCell
        
        
        //cell.workoutName.text = 
        
        return cell
    }
    
    // Vad som händer när man trycker på en rad
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Gör en övergång till ExerciseCreation
        performSegue(withIdentifier: "exerciseSegue", sender: indexPath.row)
        }
    
    // Vad den ska skicka med
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "login" {
            // Kod för påväg till login vyn
        }
        if segue.identifier == "exerciseSegue"
        {
            let destination = segue.destination as! ExerciseVC
            
            // Destinations workoutsArray är lika med denna vyns workoutArray
            //destination.workoutsArray = workoutsArray
            
            // Ge radnumret som en Int?
            //destination.workoutsArray = workoutsArray[sender as! Int]
            
            //destination.workoutName = workoutNameTextField.text
            /* dest.allExercises = exerciseList
             dest.currentExerciseNumber = sender as! Int*/
        }
        
       
    }
    
}
